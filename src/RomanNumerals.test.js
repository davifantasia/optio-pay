import RomanNumerals from './RomanNumerals';

test('converts integers to roman numerals', () => {
  expect(RomanNumerals.toRoman(1000)).toBe('M');
  expect(RomanNumerals.toRoman(900)).toBe('CM');
  expect(RomanNumerals.toRoman(90)).toBe('XC');
  expect(RomanNumerals.toRoman(1990)).toBe('MCMXC');
  expect(RomanNumerals.toRoman(2000)).toBe('MM');
  expect(RomanNumerals.toRoman(8)).toBe('VIII');
  expect(RomanNumerals.toRoman(2008)).toBe('MMVIII');
  expect(() => RomanNumerals.toRoman('20a')).toThrow('Integer is invalid.');
});

test('converts roman numerals to integers', () => {
  expect(RomanNumerals.fromRoman('M')).toBe(1000);
  expect(RomanNumerals.fromRoman('XXV')).toBe(25);
  expect(RomanNumerals.fromRoman('XIX')).toBe(19);
  expect(RomanNumerals.fromRoman('MCMXC')).toBe(1990);
  expect(RomanNumerals.fromRoman('MMVIII')).toBe(2008);
  expect(() => RomanNumerals.fromRoman('XA')).toThrow('Roman numeral is invalid.');
});