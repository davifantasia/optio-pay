export default class RomanNumerals {
  static toRoman(integer) {
    // inspired by https://stackoverflow.com/a/9083076
    if (isNaN(integer)) {
      throw new Error('Integer is invalid.');
    }

    const digits = String(+integer).split("");
    const key = [
      "","C","CC","CCC","CD","D","DC","DCC","DCCC","CM",
      "","X","XX","XXX","XL","L","LX","LXX","LXXX","XC",
      "","I","II","III","IV","V","VI","VII","VIII","IX"
    ];
    let roman = "";
    let i = 3;

    while (i--) {
      roman = (key[+digits.pop() + (i * 10)] || "") + roman;
    }

    return Array(+digits.join("") + 1).join("M") + roman;
  }

  static fromRoman(romanNumeral) {
    let sum = 0;
    const keys = { I:1, V:5, X:10, L:50, C:100, D:500, M:1000 };
    const subtractionRomans = ['I', 'X', 'C'];

    for(let i = 0; i < romanNumeral.length; i++){
      if (!keys[romanNumeral[i]]) {
        sum = undefined;
        break;
      }
      
      const nextIndex = i + 1;

      if (
        subtractionRomans.includes(romanNumeral[i])
        && romanNumeral[nextIndex]
        && keys[romanNumeral[nextIndex]] > keys[romanNumeral[i]]
      ) {
        sum -= keys[romanNumeral[i]];
      } else {
        sum += keys[romanNumeral[i]];
      }
    }

    if (!sum) {
      throw new Error('Roman numeral is invalid.');
    }

    return sum;
  }
}
