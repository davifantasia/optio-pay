import { render, screen } from '@testing-library/react';
import App from './App';

test('renders appropriate title', () => {
  render(<App />);
  const titleElement = screen.getByText(/roman numerals converter/i);
  expect(titleElement).toBeInTheDocument();
});
