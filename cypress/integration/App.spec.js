describe('Roman numeral converter', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000/');
  });

  it('converts integer to roman numeral', () => {
    cy.get('#integer').type(2008);
    cy.get('#roman-numeral').should('have.value', 'MMVIII');
    cy.contains('Integer is invalid').should('not.be.visible');
  });

  it('converts roman numeral to integer', () => {
    cy.get('#roman-numeral').type('MCMXC');
    cy.get('#integer').should('have.value', '1990');
    cy.contains('Roman Numeral is invalid').should('not.be.visible');
  });

  it('renders an error when an invalid integer is provided', () => {
    cy.get('#integer').type('2008a');
    cy.contains('Integer is invalid').should('be.visible');
  });

  it('renders an error when an invalid roman numeral is provided', () => {
    cy.get('#roman-numeral').type('MCMXC1');
    cy.contains('Roman Numeral is invalid').should('be.visible');
  });
});
