import React from 'react';
import { css } from '@emotion/react';
import styled from '@emotion/styled';
import RomanNumerals from './RomanNumerals';

const centered = css`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Header = styled.header`
  background-color: #282c34;
  padding: 1rem;
  ${centered};
  color: white;
`;

const Body = styled.div`
  ${centered};
  padding: 1rem;
`;

const Inputs = styled.div`
  display: inline-grid;
  grid-gap: 10px;
  grid-template-columns: auto auto auto;

  label {
    display: flex;
    justify-content: flex-end;
  }

  input {
    width: 10rem;
  }
`;

const InputError = styled.span`
  ${(props) => css`visibility: ${props.visibility}`}
  color: red;
`;

function App() {
  const [integer, setInteger] = React.useState('');
  const [romanNumeral, setRomanNumeral] = React.useState('');
  const [integerErrorVisibility, setIntegerErrorVisibility] = React.useState('hidden');
  const [romanErrorVisibility, setRomanErrorVisibility] = React.useState('hidden');

  return (
    <div>
      <Header>
        <h1>Roman Numerals Converter</h1>
      </Header>
      <Body aria-label="Content">
        <Inputs>
          <label htmlFor="integer">Integer:</label>
          <input
            type="text"
            id="integer"
            name="integer"
            placeholder="e.g. 1000"
            value={integer}
            onChange={handleIntegerChange}
          />
          <InputError visibility={integerErrorVisibility}>
            Integer is invalid
          </InputError>
          <label htmlFor="roman-numeral">Roman Numeral:</label>
          <input
            type="text"
            id="roman-numeral"
            name="roman-numeral"
            placeholder="e.g. M"
            value={romanNumeral}
            onChange={handleRomanNumeralChange}
          />
          <InputError visibility={romanErrorVisibility}>
            Roman Numeral is invalid
          </InputError>
        </Inputs>
      </Body>
    </div>
  );

  function handleIntegerChange(e) {
    setInteger(e.target.value);

    if (e.target.value) {
      try {
        const roman = RomanNumerals.toRoman(e.target.value);
        if (integerErrorVisibility === 'visible') {
          setIntegerErrorVisibility('hidden');
        }
        setRomanNumeral(roman);
      } catch (e) {
        setIntegerErrorVisibility('visible');
        console.error(e.message);
      }
    }
  }

  function handleRomanNumeralChange(e) {
    setRomanNumeral(e.target.value);

    if (e.target.value) {
      try {
        const integer = RomanNumerals.fromRoman(e.target.value);
        if (romanErrorVisibility === 'visible') {
          setRomanErrorVisibility('hidden');
        }
        setInteger(integer);
      } catch (e) {
        setRomanErrorVisibility('visible');
        console.error(e.message);
      }
    }
  }
}

export default App;
